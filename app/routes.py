import pickle
from app import application
from flask import render_template, request

with open('./ML/models/model.pkl', 'rb') as output:
    model = pickle.load(output)

with open('./ML/models/vectorizer.pkl', 'rb') as output:
    vectorizer = pickle.load(output)


@application.route('/')
@application.route('/home')
def home_page():
    return render_template('home.html')


@application.route('/predict', methods=['POST'])
def predict():
    text = request.form.values()
    print(type(text))
    output = model.predict(vectorizer.transform(text))[0]
    print(output)
    return render_template('home.html', prediction_text=f"It's a {output} news!!")
