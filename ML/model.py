# Libraries
import pickle
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.metrics import accuracy_score


# Text Vectorization
def vectorize(max_df_value=0.7):
    print("Vectorization in process...")
    # Initialize a TfIdf Vectorizer
    vectorizer = TfidfVectorizer(stop_words='english', max_df=max_df_value)

    # Fit and Transform train set and Transform test set
    tfidf_train = vectorizer.fit_transform(X_train)
    tf_idf_test = vectorizer.transform(X_test)
    return vectorizer, tfidf_train, tf_idf_test


# Data Splitting
def split_data(size=0.2, state_number=7):
    print(f"Splitting Data...")
    return train_test_split(data['text'], labels, test_size=size, random_state=state_number)


# Model Training
def train_model(iterations=50):
    print("Training Model...")
    # Initialize Passive Aggressive Classifier
    classifier = PassiveAggressiveClassifier(max_iter=iterations)
    classifier.fit(vectorized_train, y_train)

    # Predict on the test set and calculate accuracy
    test_predictions = classifier.predict(vectorized_test)
    score = accuracy_score(y_test, test_predictions)
    print(f"Accuracy: {round(score * 100, 2)}%")
    return classifier


# Export Model into Pickle File
def export_models():
    print(f'Exporting models...')
    with open('./models/model.pkl', 'wb') as output:
        pickle.dump(model, output)
    with open('./models/vectorizer.pkl', 'wb') as output:
        pickle.dump(tfidf_vectorizer, output)


# Main Contructor
if __name__ == "__main__":
    data = pd.read_csv('news.csv')
    labels = data['label']
    X_train, X_test, y_train, y_test = split_data()
    tfidf_vectorizer, vectorized_train, vectorized_test = vectorize()
    model = train_model()
    export_models()

